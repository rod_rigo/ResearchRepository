<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $username
 * @property string $school
 * @property string $password
 * @property string $position
 * @property int $is_admin
 * @property int $is_client
 * @property int $is_active
 * @property string $token
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 *
 * @property \App\Model\Entity\Category[] $categories
 * @property \App\Model\Entity\Project[] $projects
 * @property \App\Model\Entity\Request[] $requests
 * @property \App\Model\Entity\Status[] $statuses
 * @property \App\Model\Entity\View[] $views
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'email' => true,
        'username' => true,
        'school' => true,
        'password' => true,
        'position' => true,
        'is_admin' => true,
        'is_client' => true,
        'is_active' => true,
        'token' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'categories' => true,
        'projects' => true,
        'requests' => true,
        'statuses' => true,
        'views' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array<string>
     */
    protected $_hidden = [
        'password',
    ];

    protected function _setPassword($value){
        return (strlen($value) > 0)? (new DefaultPasswordHasher())->hash($value): (new DefaultPasswordHasher())->hash(uniqid());
    }

    protected function _setName($value){
        return strtoupper($value);
    }

    protected function _setSchool($value){
        return strtoupper($value);
    }

    protected function _setPosition($value){
        return strtoupper($value);
    }

}
