<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>Sign</b>-Up</a>
    </div>
    <div class="card">
        <div class="card-body register-card-body">
            <p class="login-box-msg">Register a new membership</p>
            <?=$this->Form->create($user,['id' => 'form', 'type' => 'file'])?>
                <div class="input-group mb-3">
                    <?=$this->Form->text('name',[
                        'id' => 'name',
                        'required' => true,
                        'class' => 'form-control',
                        'placeholder' => ucwords('name'),
                        'title' => ucwords('please fill out this field')
                    ])?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <?=$this->Form->select('position', $positions,[
                        'id' => 'position',
                        'required' => true,
                        'class' => 'form-control',
                        'empty' => ucwords('position'),
                        'title' => ucwords('please fill out this field')
                    ])?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user-tie"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <?=$this->Form->email('email',[
                        'id' => 'email',
                        'required' => true,
                        'class' => 'form-control',
                        'placeholder' => ucwords('email'),
                        'title' => ucwords('please fill out this field')
                    ])?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <?=$this->Form->text('username',[
                        'id' => 'username',
                        'required' => true,
                        'class' => 'form-control',
                        'placeholder' => ucwords('Username'),
                        'title' => ucwords('please fill out this field')
                    ])?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-user"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <?=$this->Form->select('school', $schools,[
                        'id' => 'school',
                        'required' => true,
                        'class' => 'form-control',
                        'empty' => ucwords('school'),
                        'title' => ucwords('please fill out this field')
                    ])?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-building"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <?=$this->Form->password('password',[
                        'id' => 'password',
                        'required' => true,
                        'class' => 'form-control',
                        'placeholder' => ucwords('password'),
                        'title' => ucwords('please fill out this field')
                    ])?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <div class="input-group mb-3">
                    <?=$this->Form->password('confirm_password',[
                        'id' => 'confirm-password',
                        'required' => true,
                        'class' => 'form-control',
                        'placeholder' => ucwords('confirm password'),
                        'title' => ucwords('please fill out this field')
                    ])?>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-8 d-flex justify-content-start align-items-center">
                        <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'login'])?>" turbolink class="text-center">
                            Return To Login
                        </a>
                    </div>
                    <div class="col-4">
                        <?=$this->Form->hidden('is_admin',[
                            'required' => true,
                            'value' => intval(0),
                            'id' => 'is-admin'
                        ])?>
                        <?=$this->Form->hidden('is_client',[
                            'required' => true,
                            'value' => intval(1),
                            'id' => 'is-client'
                        ])?>
                        <?=$this->Form->button('Submit',[
                            'class' => 'btn btn-primary btn-block',
                            'type' => 'submit'
                        ])?>
                    </div>
                </div>
            <?=$this->Form->end()?>
        </div>

    </div>
</div>
<?=$this->Html->script('users/register')?>
