<?php
/**
 * @var \App\View\AppView $this
 */
?>

<div class="register-box">
    <div class="register-logo">
        <a href="#"><b>Verification</b></a>
    </div>
    <div class="card">
        <div class="card-body register-card-body">
            <p class="login-box-msg">No Email Yet? Please Check Your Spam Messages</p>
            <?=$this->Form->create($user,['id' => 'form', 'type' => 'file'])?>
            <div class="input-group mb-3">
                <?=$this->Form->password('code',[
                    'id' => 'code',
                    'required' => true,
                    'class' => 'form-control',
                    'placeholder' => ucwords('code'),
                    'title' => ucwords('please fill out this field'),
                ])?>
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fa fa-user-cog"></span>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-8 d-flex justify-content-start align-items-center">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'login'])?>" turbolink class="text-center">
                        Return To Login
                    </a>
                </div>
                <div class="col-4">
                    <?=$this->Form->hidden('is_admin',[
                        'required' => true,
                        'value' => intval(0),
                        'id' => 'is-admin'
                    ])?>
                    <?=$this->Form->hidden('is_client',[
                        'required' => true,
                        'value' => intval(1),
                        'id' => 'is-client'
                    ])?>
                    <?=$this->Form->button('Submit',[
                        'class' => 'btn btn-primary btn-block',
                        'type' => 'submit'
                    ])?>
                </div>
            </div>
            <?=$this->Form->end()?>
        </div>

    </div>
</div>
<?=$this->Html->script('users/verify')?>
