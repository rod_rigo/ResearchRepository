<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'Research Repository';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <meta name="turbolinks-cache-control" content="public">
    <meta name="turbolinks-visit-control" content="reload">
    <?= $this->Html->meta('csrf-token',$this->request->getAttribute('csrfToken')) ?>

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@500;600&family=Roboto&display=swap" rel="stylesheet">

    <?=$this->Html->css('https://use.fontawesome.com/releases/v5.15.4/css/all.css')?>
    <?=$this->Html->css('https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css')?>
    <?=$this->Html->css([
        '/environs/lib/owlcarousel/assets/owl.carousel.min',
        '/environs/lib/lightbox/css/lightbox.min',
        '/environs/css/bootstrap.min',
        '/environs/css/style',
        '/jquery/css/jquery-ui',
        '/i-check/css/icheck-bootstrap',
    ])?>

    <?=$this->Html->script([
        '/jquery/js/jquery-3.6.0',
        '/jquery/js/jquery-ui',
        '/moment/js/moment',
        '/sweet-alert/js/sweetalert2.all',
        '/sweet-alert/js/sweetalert2',
        '/turbo-links/js/turbolinks',
    ])?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

    <script>
        var mainurl = window.location.origin+'/ResearchRepository/';
    </script>

</head>
<body>

<!-- Spinner Start -->
<div id="spinner" class="show w-100 vh-100 bg-white position-fixed translate-middle top-50 start-50  d-flex align-items-center justify-content-center">
    <div class="spinner-grow text-primary" role="status"></div>
</div>
<!-- Spinner End -->

<!-- Navbar start -->
<?=$this->element('website/navbar')?>
<!-- Navbar End -->

<?= $this->Flash->render() ?>
<?= $this->fetch('content') ?>

<?=$this->element('website/footer')?>

<?=$this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js')?>
<?=$this->Html->script('https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js')?>
<?=$this->Html->script([
    '/environs/lib/easing/easing.min',
    '/environs/lib/waypoints/waypoints.min',
    '/environs/lib/counterup/counterup.min',
    '/environs/lib/owlcarousel/owl.carousel.min',
    '/environs/lib/lightbox/js/lightbox.min',
    '/environs/js/main'
])?>

<script>
    Turbolinks.start();
    $('a[turbolink]').click(function (e) {
        var href = $(this).attr('href');
        Turbolinks.visit(href,{action:'replace'});
    });
</script>

</body>
</html>
