<?php
/**
 * @var \App\View\AppView $this
 */
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
        <img src="<?=$this->Url->assetUrl('/img/deped.png')?>" class="img-circle w-75" loading="lazy" title="Deped Logo" alt="DEPED">
    </div>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-header">Navigation</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Client', 'controller' => 'Dashboards', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('dashboards') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Client', 'controller' => 'Users', 'action' => 'account', intval(@$auth['id'])])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('users') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Account
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Client', 'controller' => 'Projects', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('projects') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-file-archive"></i>
                        <p>
                            Projects
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Client', 'controller' => 'Requests', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('requests') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-pencil-alt"></i>
                        <p>
                            Requests
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Sign Out
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
