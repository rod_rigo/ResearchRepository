<?php
/**
 * @var \App\View\AppView $this
 */
?>

<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex justify-content-center align-items-center">
        <img src="<?=$this->Url->assetUrl('/img/deped.png')?>" class="img-circle w-75" loading="lazy" title="Deped Logo" alt="DEPED">
    </div>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->

                <li class="nav-header">Navigation</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Dashboards', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('dashboards') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Users', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('users') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Users
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Statuses', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('statuses') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ul"></i>
                        <p>
                            Statuses
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Categories', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('categories') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>
                            Categories
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Positions', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('positions') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            Positions
                        </p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('projects') && !in_array(strtolower($action),[strtolower('bin')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('projects') && !in_array(strtolower($action),[strtolower('bin')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-file-archive"></i>
                        <p>
                            Projects
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('projects') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'completed'])?>" class="nav-link <?=(strtolower($controller) == strtolower('projects') && strtolower($action) == strtolower('completed') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Completed</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'ongoing'])?>" class="nav-link <?=(strtolower($controller) == strtolower('projects') && strtolower($action) == strtolower('ongoing') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ongoing</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'expired'])?>" class="nav-link <?=(strtolower($controller) == strtolower('projects') && strtolower($action) == strtolower('expired') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Expired</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Schools', 'action' => 'index'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('schools') && strtolower('bin') != strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Schools
                        </p>
                    </a>
                </li>
                <li class="nav-item <?=(strtolower($controller) == strtolower('transactions') && !in_array(strtolower($action),[strtolower('bin')]))? 'menu-is-opening menu-open': null;?>">
                    <a href="javascript:void(0);" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && !in_array(strtolower($action),[strtolower('bin'), strtolower('released'), strtolower('received')]))? 'active': null;?>">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Requests
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'index'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('index') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'today'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('today') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Today</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'week'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('week') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Week</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'month'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('month') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Month</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a turbolink href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Requests', 'action' => 'year'])?>" class="nav-link <?=(strtolower($controller) == strtolower('transactions') && strtolower($action) == strtolower('year') && strtolower('bin') != strtolower($action))? 'active': null;?>">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Year</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header mt-2">Bin</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Statuses', 'action' => 'bin'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('statuses') && strtolower('bin') == strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ul"></i>
                        <p>
                            Statuses
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Categories', 'action' => 'bin'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('categories') && strtolower('bin') == strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-list-ol"></i>
                        <p>
                            Categories
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Positions', 'action' => 'bin'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('positions') && strtolower('bin') == strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            Positions
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Projects', 'action' => 'bin'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('projects') && strtolower('bin') == strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-file-archive"></i>
                        <p>
                            Projects
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => 'Admin', 'controller' => 'Schools', 'action' => 'bin'])?>" turbolink class="nav-link <?=strtolower($controller) == strtolower('schools') && strtolower('bin') == strtolower($action)? 'active': null;?>">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Schools
                        </p>
                    </a>
                </li>

                <li class="nav-header mt-2">Others</li>
                <li class="nav-item">
                    <a href="<?=$this->Url->build(['prefix' => false, 'controller' => 'Users', 'action' => 'logout'])?>" turbolink class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Sign Out
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
