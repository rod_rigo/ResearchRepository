<?php
/**
 * @var \App\View\AppView $this
 * @var array $params
 * @var string $message
 */
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<script>
    'use strict';
    $(function (e) {
        Swal.fire({
            icon:'error',
            title: '<?= ucwords($message) ?>',
            text:null,
            showConfirmButton:false,
            timer:3000,
            showTimerProgressBar:false,
            toast:true,
            position: 'top-right'
        });
    });
</script>
