<?php
/**
 * @var \App\View\AppView $this
 * @var iterable<\App\Model\Entity\Project> $projects
 */
?>

<section class="content-header">
    <div class="container-fluid">
        <h2 class="text-center display-4">Search Project</h2>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <?=$this->Form->create(null,['id' => 'form', 'type' => 'file', 'method' => 'get'])?>
                <div class="input-group input-group-lg">
                    <input type="search" name="search" class="form-control form-control-lg" placeholder="Type Title Or Author Here" value="<?=$search?>">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-lg btn-default">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
                <?=$this->Form->end()?>
            </div>
        </div>
        <?php if (!$projects->isEmpty()):?>
            <div class="row mt-3">
                <?php foreach ($projects as $project):?>
                    <div class="col-md-10 offset-1 mt-2 cursor-pointer">
                        <div class="list-group">
                            <div class="list-group-item">
                                <div class="row">
                                    <div class="col px-4">
                                        <div>
                                            <div class="float-right">
                                                <?php if(intval(strlen($project->date)) > intval(0)):?>
                                                    <?=date('Y-M-D h:i A', strtotime($project->date))?>
                                                <?php else:?>
                                                <?php endif;?>
                                            </div>
                                            <h3><?=strtoupper($project->title)?></h3>
                                            <p class="mb-0">
                                                <?=strtoupper($project->author).', '. $project->batch?>
                                                <a href="<?=$this->Url->build(['prefix' => 'Client', 'controller' => 'Projects', 'action' => 'view', intval($project->id), '?'=> ['token' => microtime()]])?>" class="float-right" turbolink>View</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
            <div class="row mt-1">
                <div class="col-sm-12 col-md-5 col-lg-5 mt-2 offset-1">
                    <?php
                        $this->Paginator->setTemplates([
                            'first' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>',
                            'prevActive' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>',
                            'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>',
                            'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                            'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
                            'ellipsis' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a></li>',
                            'last' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>',
                            'nextActive' => '<li class="page-item"><a class="page-link" href="{{url}}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>',
                            'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="{{url}}"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>',
                        ]);
                    ?>
                    <ul class="pagination d-flex justify-content-start align-items-center">
                        <?= $this->Paginator->first();?>
                        <?= $this->Paginator->prev();?>
                        <?= $this->Paginator->numbers();?>
                        <?= $this->Paginator->next();?>
                        <?= $this->Paginator->last();?>
                    </ul>
                </div>
                <div class="col-sm-12 col-md-5 col-lg-5 mt-2 text-capitalize offset-1 h6">
                    <?= $this->Paginator->counter('Page {{page}} of {{pages}}, showing {{current}} records out of {{count}} total, starting on record {{start}}, ending on {{end}}'); ?>
                </div>
            </div>
        <?php else:?>

        <?php endif;?>
    </div>
</section>