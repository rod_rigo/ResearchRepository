'use strict';
$(document).ready(function (e) {

    var baseurl = mainurl+'requests/';
    var url;
    var skip = 10;
    var limit = 10;

    $('#form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: baseurl+'add',
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                Swal.fire({
                    icon: 'info',
                    title: null,
                    text: 'Please Wait!...',
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    timerProgressBar: false,
                    didOpen: function () {
                        Swal.showLoading();
                    }
                });
                $('.form-control').removeClass('is-invalid');
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
            },
        }).done(function (data, status, xhr) {
            $('#form')[0].reset();
            swal('success', null, data.message);
            Turbolinks.visit(data.redirect,{action: 'advance'});
        }).fail(function (data, status, xhr) {
            const errors = data.responseJSON.errors;

            swal('warning', null, data.responseJSON.message);

            $.map(errors, function (value, key) {
                var name = key;
                $.map(value, function (value, key) {
                    $('[name="'+(name)+'"]').addClass('is-invalid');
                    $('[name="'+(name)+'"]').next('small').text(value);
                });
            });

            $('button[type="submit"], button[type="reset"]').prop('disabled', false);

        });
    });

    $('#comment-form').submit(function (e) {
        e.preventDefault();
        var data = new FormData(this);
        $.ajax({
            url: mainurl+'comments/add',
            type: 'POST',
            method: 'POST',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            dataType: 'JSON',
            beforeSend: function (e) {
                $('.form-control').removeClass('is-invalid');
                $('button[type="submit"], button[type="reset"]').prop('disabled', true);
                $('#comment-form button[type="submit"]').html('<i class="fa fa-spinner fa-pulse"></i>');
            },
        }).done(function (data, status, xhr) {
            $('#comment-form')[0].reset();
            $('#comment-form button[type="submit"]').html('<i class="fa fa-paper-plane" aria-hidden="true"></i>');
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
            var html = '<div class="post">'+
                '<div class="user-block">'+
                '<span class="username">'+
                '<a href="javascript:void(0);">'+(data.user)+'</a>'+
                '</span>'+
                '<span class="description">'+(moment(data.comment.created).fromNow(true))+'</span>'+
                '</div>'+
                '<p>'+
                (data.comment.comment)+
                '</p>'+
                '</div>';
            $('#comments').append(html);
        }).fail(function (data, status, xhr) {
            $('button[type="submit"], button[type="reset"]').prop('disabled', false);
        });
    });
    
    $('#toggle-modal').click(function (e) {
        $('#modal').modal('toggle');
    });
    
    $('#modal').on('hidden.bs.modal', function (e) {
        $('#form')[0].reset();
    });
    
    $('#password').on('input', function (e) {
        var regex = /(.){1,}/;
        var value = $(this).val();

        if(!value.match(regex)){
            $(this).addClass('is-invalid').next('small').text('Please Enter Your Password');
            return true;
        }

        $(this).removeClass('is-invalid').next('small').empty();
    });

    $('#agreed').change(function (e) {
        var checked = $(this).prop('checked');
        $('#is-agreed').val(Number(checked));
    });

    $('#load-comments').click(function (e) {
        $(this).addClass('cursor-wait');
        setTimeout(function () {
            getComments();
        }, 1500);
    });

    $('#rating').starRating({
        totalStars: 5,
        starSize: 50,
        emptyColor: 'lightgray',
        hoverColor: '#FFD700',
        activeColor: '#FFD700',
        initialRating: parseFloat(rate),
        strokeWidth: 0,
        useGradient: false,
        callback: function(points){
            ratings(points);
        }
    });

    function getComments() {
        $.ajax({
            url: mainurl+'comments/getComments/'+(projectId)+'/'+(skip)+'/'+(limit),
            type: 'GET',
            method: 'GET',
            dataType: 'JSON',
            beforeSend:function () {

            },
        }).done(function (data, status, xhr) {
            $.map(data, function (data, key) {
                comment(data);
            });
            skip+= 10;
            $('#load-comments').removeClass('cursor-wait');
        }).fail(function (data, status, xhr) {

        });
    }

    function comment(data) {
        var html = '<div class="post">'+
            '<div class="user-block">'+
            '<span class="username">'+
            '<a href="javascript:void(0);">'+(data.user.name)+'</a>'+
            '</span>'+
            '<span class="description">'+(moment(data.created).fromNow(true))+'</span>'+
            '</div>'+
            '<p>'+
            (data.comment)+
            '</p>'+
            '</div>';
        $('#comments').append(html);
    }

    function ratings(points) {
        var data = {
            'user_id' : parseInt(userId),
            'project_id' : parseInt(projectId),
            'rating' : parseFloat(points),
        };
        $.ajax({
            url: mainurl+'ratings/add',
            type: 'POST',
            method: 'POST',
            data: JSON.stringify(data),
            contentType: 'application/json', // Set content type to JSON
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function (e) {

            },
        }).done(function (data, status, xhr) {

        }).fail(function (data, status, xhr) {

        });
    }

    function swal(icon, result, message){
        Swal.fire({
            icon: icon,
            title: result,
            text: message,
            timer: 5000,
            timerProgressBar:true,
        });
    }
    
});