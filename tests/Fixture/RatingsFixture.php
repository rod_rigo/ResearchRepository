<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RatingsFixture
 */
class RatingsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'project_id' => 1,
                'rating' => 1,
                'created' => 1712716945,
                'modified' => 1712716945,
                'deleted' => '2024-04-10 10:42:25',
            ],
        ];
        parent::init();
    }
}
