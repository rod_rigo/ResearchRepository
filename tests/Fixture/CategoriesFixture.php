<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CategoriesFixture
 */
class CategoriesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'category' => 'Lorem ipsum dolor sit amet',
                'created' => 1712041945,
                'modified' => 1712041945,
                'deleted' => '2024-04-02 15:12:25',
            ],
        ];
        parent::init();
    }
}
