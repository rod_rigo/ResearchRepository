<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StatusesFixture
 */
class StatusesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'status' => 'Lorem ipsum dolor sit amet',
                'is_default' => 1,
                'created' => 1712041672,
                'modified' => 1712041672,
                'deleted' => '2024-04-02 15:07:52',
            ],
        ];
        parent::init();
    }
}
