<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PositionsFixture
 */
class PositionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'position' => 'Lorem ipsum dolor sit amet',
                'created' => 1712107073,
                'modified' => 1712107073,
                'deleted' => '2024-04-03 09:17:53',
            ],
        ];
        parent::init();
    }
}
