<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SchoolsFixture
 */
class SchoolsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'school' => 'Lorem ipsum dolor sit amet',
                'created' => 1712713264,
                'modified' => 1712713264,
                'deleted' => '2024-04-10 09:41:04',
            ],
        ];
        parent::init();
    }
}
